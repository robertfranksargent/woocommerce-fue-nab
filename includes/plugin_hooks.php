<?php

// install
register_activation_hook(basename(FUE_NAB_DIR).'/'. basename(FUE_NAB_DIR) .'.php', 'FollowUpEmailsNab::install');

// uninstall
register_deactivation_hook(basename(FUE_NAB_DIR).'/'. basename(FUE_NAB_DIR) .'.php', 'FollowUpEmailsNab::uninstall');

// Upgrade
add_action( 'plugins_loaded', array( 'FollowUpEmailsNab', 'update_db' ) );

// menu
add_action('admin_menu', 'FUE_NAB_Admin::add_menu', 21);

// load addons
add_action('plugins_loaded', 'FollowUpEmailsNab::load_addons');

// Apply to Existing Subscriptions
add_action('wp_ajax_fue_nab_apply_to_existing_subscriptions', 'FollowUpEmailsNab::ajax_apply_to_existing_subscriptions');
