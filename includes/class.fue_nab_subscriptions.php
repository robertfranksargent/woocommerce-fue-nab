<?php

$GLOBALS['fue_subscriptions_product_link'] = 'http://www.75nineteen.com/woocommerce';

class FUE_NAB_Subscriptions {

    static $license_product = 'subscriptions';
    static $platform        = 'woocommerce';

    public function __construct() {

        if ( self::is_installed() ) {

            // filters
            add_filter( 'fue_nab_add_triggers', array($this, 'filter_add_triggers'), 10, 1 );

            add_filter( 'fue_nab_email_variables_list', array($this, 'filter_email_variables_list'), 10, 1 );
            add_filter( 'fue_email_manual_variables_list', array($this, 'action_email_variables_list'), 10, 1 );

            add_filter( 'fue_nab_email_variables', array($this, 'filter_email_variables'), 10, 4 );
            add_filter( 'fue_email_manual_variables', array($this, 'filter_email_variables'), 10, 4 );

            add_filter( 'fue_nab_email_replacements', array($this, 'filter_email_replacements'), 10, 4 );
            add_filter( 'fue_email_manual_replacements',  array($this, 'filter_email_replacements'), 10, 4 );

            add_filter( 'fue_nab_email_test_variables', array($this, 'filter_email_test_variables'), 10, 1 );
            add_filter( 'fue_email_manual_test_variables', array($this, 'filter_email_test_variables'), 10, 1 );

            add_filter( 'fue_nab_email_test_replacements', array($this, 'filter_email_test_replacements'), 10, 1 );
            add_filter( 'fue_email_manual_test_replacements',  array($this, 'filter_email_test_replacements'), 10, 1 );

            // actions for failed payment emails
            add_action( 'processed_subscription_payment_failure', array($this, 'processed_subscription_payment_failure'), 10, 2 );
            add_action( 'processed_subscription_payment', array($this, 'processed_subscription_payment'), 10, 2 );

            // action for subscription cancelled and expired
            add_action( 'subscription_end_of_prepaid_term', array($this, 'subscription_end_of_prepaid_term'), 10, 2 );

            // expiry date updated
            add_filter( 'woocommerce_subscriptions_set_expiration_date',  array($this, 'filter_woocommerce_subscriptions_set_expiration_date'), 10, 4 );
            add_action( 'fue_nab_update_expiration_date',  array($this, 'action_fue_nab_update_expiration_date'), 10, 3 );

            // manual send actions
            add_action( 'fue_manual_types', array($this, 'manual_types'), 20, 0 );
            add_action( 'fue_manual_type_actions', array($this, 'manual_type_actions'), 20, 0 );
            add_action( 'fue_manual_js', array($this, 'manual_js') );

            add_filter( 'fue_send_manual_emails', array($this, 'send_manual_email'), 10, 2 );

        }

    }

    public static function is_installed() {

        //return ( class_exists( 'WC_Subscriptions' ) );
        
        // this wasn't working, so just assume Subscriptions are active
        
        return true;

    }

    public static function filter_add_triggers( $triggers ) {
        $triggers['subs_payment_failed']        = __('after a subscription payment failed', 'follow_up_emails_nab');
        $triggers['subs_payment_failed_3times'] = __('after a subscription payment failed 3 times', 'follow_up_emails_nab');
        $triggers['subs_cancelled_and_expired'] = __('after a subscription that was cancelled expires', 'follow_up_emails_nab');

        return $triggers;
    }

    public static function filter_email_variables_list( $variables_list ) {
        $variables_list .= '<li class="var hideable var_recipient_1_name"><strong>{recipient_1_name}</strong> <img class="help_tip" title="' . translate('The first name of the first recipient.', 'follow_up_emails_nab') . '" src="' . FUE_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";
        $variables_list .= '<li class="var hideable var_recipient_names"><strong>{recipient_names}</strong> <img class="help_tip" title="' . translate('The first name(s) of the recipient(s).', 'follow_up_emails_nab') . '" src="' . FUE_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";
        $variables_list .= '<li class="var hideable var_next_dispatch"><strong>{next_dispatch}</strong> <img class="help_tip" title="' . translate('The next dispatch date.', 'follow_up_emails_nab') . '" src="' . FUE_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";
        //$variables_list .= '<li class="var hideable var_last_present_1"><strong>{last_present_1}</strong> <img class="help_tip" title="' . translate('The last present.', 'follow_up_emails_nab') . '" src="' . FUE_NAB_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";
        //$variables_list .= '<li class="var hideable var_last_present_2"><strong>{last_present_2}</strong> <img class="help_tip" title="' . translate('The second last present.', 'follow_up_emails_nab') . '" src="' . FUE_NAB_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";
        //$variables_list .= '<li class="var hideable var_last_present_3"><strong>{last_present_3}</strong> <img class="help_tip" title="' . translate('The third last present.', 'follow_up_emails_nab') . '" src="' . FUE_NAB_TEMPLATES_URL . '/images/help.png" width="16" height="16" /></li>' . "\n";

        return $variables_list;
    }

    public static function action_email_variables_list() {
        echo self::filter_email_variables_list('');
    }

    public static function filter_email_variables( $vars, $email_data, $email_order, $email_row ) {

        $vars = array_merge($vars, array('{recipient_1_name}', '{recipient_names}', '{next_dispatch}'/*, '{last_present_1}', '{last_present_2}', '{last_present_3}'*/));

        return $vars;
    }

    public static function filter_email_replacements( $reps, $email_data, $email_order, $email_row ) {
        global $wpdb, $woocommerce;

        if ( $email_order->order_id ) {
            $order      = WC_FUE_Compatibility::wc_get_order( $email_order->order_id );

            $item       = WC_Subscriptions_Order::get_item_by_product_id($order);
            $item_id    = WC_Subscriptions_Order::get_items_product_id($item);

            // update customer names for subs with bad data

            // if for gift
            if ( isset($item['Recipient']) and $item['Recipient'] == 'For gift' ) {

                // if billing address matches shipping address
                //if ( ) {

                    $reps['customer_first_name'] = 'Subscriber';
                    $reps['customer_name']       = 'Subscriber';

                //}

            }

            // add new variables

            $recipient_1_name = $item['Recipient Name 1 - Text'];
            $recipient_1_name = ucwords( strtolower( $recipient_1_name ) );

            $recipient_names  = $item['Recipient Name 1 - Text'] . ( ( !empty($item['Recipient Name 2 - Text']) ) ? " &amp; {$item['Recipient Name 2 - Text']}" : '' );
            $recipient_names  = ucwords( strtolower( $recipient_names ) );

            $next_dispatch    = WCS_Utils::next_shipping_date();


            $reps = array_merge($reps, array(
                'recipient_1_name'  => $recipient_1_name,
                'recipient_names'   => $recipient_names,
                'next_dispatch'     => $next_dispatch
                /*,
                ($last_present[1]) ?: '',
                ($last_present[2]) ?: '',
                ($last_present[3]) ?: ''
                */
            ));

        }

        return $reps;
    }

    public static function filter_email_test_variables( $vars ) {
        FollowUpEmailsNab::log('filter_email_test_variables');

        $vars = array_merge($vars, array('{recipient_1_name}', '{recipient_names}', '{next_dispatch}', '{last_present_1}', '{last_present_2}', '{last_present_3}'));

        return $vars;
    }

    public static function filter_email_test_replacements( $reps ) {
        FollowUpEmailsNab::log('filter_email_test_replacements');

        $recipient_1_name = 'Lilly';
        $recipient_names  = 'Lilly & Alan';
        $next_dispatch    = '6th July';
        $last_present_1   = 'Hikaru NOGUCHI GLOVES';
        $last_present_2   = 'Half Moon Wall Calendar';
        $last_present_3   = 'FERM BRASS OPENER';

        /*$category = get_field( 'subscriptions_button_link' ) ;
        $products = new WP_Query( ) ;
        $products->query_vars = array(
            'posts_per_page' => 3 ,
            'post_type' => 'product' ,
            'post_status' => 'publish' ,
            //'product_cat' => $category->slug ,
            //'meta_key' => '_featured' ,
            //'meta_query' => array( 
            //    array(
            //        'key' => '_featured'
            //    )
            //) ,
            'orderby' => 'meta_value date'
        ) ;
        $products->get_posts( ) ;

        if ( $products->have_posts( ) ) :
            $i = 1;
            foreach ( $products->posts as $post ) :
                $last_present[$i] = $post->post_title;
                $i++;
            endforeach;
        endif;*/

                                    
        $reps = array_merge($reps, array(
            'recipient_1_name'  => $recipient_1_name,
            'recipient_names'   => $recipient_names,
            'next_dispatch'     => $next_dispatch,
            'last_present_1'    => $last_present_1,
            'last_present_2'    => $last_present_2,
            'last_present_3'    => $last_present_3,
            /*
            ($last_present[1]) ?: '',
            ($last_present[2]) ?: '',
            ($last_present[3]) ?: ''
            */
        ));

        return $reps;
    }

    public static function processed_subscription_payment_failure($user_id, $subscription_key) {
        global $wpdb;

        $parts    = explode('_', $subscription_key);
        $order_id = $parts[0];

        // check db for previous failed payments
        if ( $failed_payments = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_failed_payments` WHERE `subscription_id` = '%d'", $order_id) ) ) {
            $times = $failed_payments->times + 1;

            if ($times < 3) {
                $triggers[] = 'subs_payment_failed';
            }

            else if ($times >= 3) {
                $triggers[] = 'subs_payment_failed_3times';
            }

            // save failed payment
            $wpdb->update("{$wpdb->prefix}followup_failed_payments", array('times' => $times, 'date' => 'NOW()'), array('subscription_id' => $order_id));

        } else {

            $triggers[] = 'subs_payment_failed';

            // save failed payment
            $wpdb->insert("{$wpdb->prefix}followup_failed_payments", array('subscription_id' => $order_id, 'times' => 1, 'date' => 'NOW()'));

        }

        FUE_Subscriptions::create_email_orders($order_id, $triggers, $subscription_key, $user_id);
    }

    public static function processed_subscription_payment($user_id, $subscription_id) {
        global $wpdb;

        // check db for previous failed payments
        if ( $failed_payments = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_failed_payments` WHERE `subscription_id` = '%d'", $subscription_id) ) ) {

            // delete row
            $wpdb->delete("{$wpdb->prefix}followup_failed_payments", array('subscription_id' => $subscription_id));

        }

    }

    public static function subscription_end_of_prepaid_term($user_id, $subscription_key) {
        global $wpdb;

        $parts    = explode('_', $subscription_key);
        $order_id = $parts[0];

        $triggers[] = 'subs_cancelled_and_expired';

        FUE_Subscriptions::create_email_orders($order_id, $triggers, $subscription_key, $user_id);
    }


    public static function filter_woocommerce_subscriptions_set_expiration_date($value, $expiration_date, $subscription_key, $user_id) {

        do_action('fue_nab_update_expiration_date', $expiration_date, $subscription_key, $user_id);

        return $value;

    }

    public static function action_fue_nab_update_expiration_date($expiration_date, $subs_key, $user_id) {
        global $wpdb;

        $parts = explode('_', $subs_key);
        $order_id       = $parts[0];
        $product_id     = $parts[1];


        // delete queued emails with the same order id
        // and the 'subs_before_expire' trigger

        $rows = $wpdb->get_results( $wpdb->prepare("
            SELECT eo.id
            FROM {$wpdb->prefix}followup_email_orders eo, {$wpdb->prefix}followup_emails e
            WHERE eo.is_sent = 0
            AND eo.order_id = %d
            AND eo.email_id = e.id
            AND e.interval_type IN ('subs_before_expire')
        ", $order_id) );

        if ( $rows ) {
            foreach ( $rows as $row ) {
                $wpdb->query( $wpdb->prepare("DELETE FROM {$wpdb->prefix}followup_email_orders WHERE id = %d", $row->id) );
            }
        }


        // add new email orders

        $GLOBALS['fue_subscriptions']->set_expiration_reminder($user_id, $subs_key);

    }

    /*
     * This is not required yet, as we aren't changing payment dates. but we may do later.
     *
    public static function action_fue_nab_update_payment_date($expiration_date, $subs_key, $user_id) {
        global $wpdb;

        $parts = explode('_', $subs_key);
        $order_id       = $parts[0];
        $product_id     = $parts[1];


        // delete queued emails with the same order id
        // and the 'subs_before_renewal' trigger

        $rows = $wpdb->get_results( $wpdb->prepare("
            SELECT eo.id
            FROM {$wpdb->prefix}followup_email_orders eo, {$wpdb->prefix}followup_emails e
            WHERE eo.is_sent = 0
            AND eo.order_id = %d
            AND eo.email_id = e.id
            AND e.interval_type IN ('subs_before_renewal')
        ", $order_id) );

        if ( $rows ) {
            foreach ( $rows as $row ) {
                $wpdb->query( $wpdb->prepare("DELETE FROM {$wpdb->prefix}followup_email_orders WHERE id = %d", $row->id) );
            }
        }


        // add new email orders

        $GLOBALS['fue_subscriptions']->set_renewal_reminder($user_id, $subs_key);

    }*/

    public function manual_types() {
        ?>
        <option value="expired_subscription"><?php _e('Customers with an expired subscription', 'follow_up_emails_nab'); ?></option>
        <option value="expired_subscription_timeframe"><?php _e('Customers who\'s last subscription expired between these dates', 'follow_up_emails_nab'); ?></option>
        <?php
    }

    public function manual_type_actions() {
        $subscriptions = array();

        $posts = get_posts( array(
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'posts_per_page' => -1
        ) );

        foreach ($posts as $post) {
            $product = sfn_get_product( $post->ID );

            if ( $product->is_type( array( WC_Subscriptions::$name, 'subscription_variation', 'variable-subscription' ) ) )
                $subscriptions[] = $product;
        }

        ?>

        <div class="send-type-expired-subscription send-type-div">
            <select id="subscription_id" name="subscription_id" class="chzn-select" style="width: 400px;">
                <?php foreach ( $subscriptions as $subscription ): ?>
                <option value="<?php echo $subscription->id; ?>"><?php echo esc_html( $subscription->get_title() ); ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="send-type-expired-subscription-timeframe send-type-div">
            <?php _e('From:', 'follow_up_emails'); ?>
            <input type="text" class="" name="expired_timeframe_from" id="expired_timeframe_from" />

            <?php _e('To:', 'follow_up_emails'); ?>
            <input type="text" class="" name="expired_timeframe_to" id="expired_timeframe_to" />
        </div>

        <?php
    }

    public function manual_js() {
        ?>
        jQuery("#send_type").change(function() {

            switch (jQuery(this).val()) {

                case "expired_subscription":
                    jQuery(".send-type-expired-subscription").show();
                    break;

                case "expired_subscription_timeframe":
                    jQuery(".send-type-expired-subscription-timeframe").show();
                    break;

            }
        });
        <?php
    }

    public function send_manual_email( $recipients, $post ) {
        global $wpdb;

        if ( $post['send_type'] == 'expired_subscription' ) {

            $subscriptions = WC_Subscriptions_Manager::get_all_users_subscriptions();

            foreach ( $subscriptions as $user_id => $user_subscriptions ) {

                $user = new WP_User( $user_id );

                array_filter($user_subscriptions, function($s) {
                    if ( $s['end_date'] ) return true;
                    else return false;
                });

                if ( count($user_subscriptions) ) {

                    uasort ($user_subscriptions, function($a, $b) {
                        if ( $a['end_date'] == $b['end_date'] ) return 0;
                        return ( $a['end_date'] > $b['end_date'] ) ? 1 : -1;
                    });

                    $subscription = end($user_subscriptions);
                    $sub_key = key($user_subscriptions);

                    // for each user get their subscription that expired last

                    // foreach ( $user_subscriptions as $sub_key => $subscription ) {

                    if ( $subscription['status'] === "expired" && ( $subscription['product_id'] == $post['subscription_id'] || $subscription['variation_id'] == $post['subscription_id'] ) ) {
                    
                        $recipient = (object) array(
                            'user_id' => 0,
                            'email'   => $user->user_email,
                            'name'    => $first_name .' '. $last_name
                        );

                        if ($recipient = apply_filters('fue_nab_expired_subscriber', $recipient, $sub_key, $subscription, $post, $user_id)) {
                            $key = $recipient->user_id .'|'. $recipient->email .'|'. $recipient->name . '|' . $sub_key;
                            $recipients[$key] = array($recipient->user_id, $recipient->email, $recipient->name, 'subscription_key' => $sub_key);
                        }

                    }

                }

            }

        }

        if ( $post['send_type'] == 'expired_subscription_timeframe' ) {

            $from_ts    = strtotime($post['expired_timeframe_from']);
            $to_ts      = strtotime($post['expired_timeframe_to']);

            $from       = date('Y-m-d', $from_ts) . ' 00:00:00';
            $to         = date('Y-m-d', $to_ts) .' 23:59:59';

            $subscriptions = WC_Subscriptions_Manager::get_all_users_subscriptions();

            foreach ( $subscriptions as $user_id => $user_subscriptions ) {

                $user = new WP_User( $user_id );

                array_filter($user_subscriptions, function($s) {
                    if ( $s['end_date'] ) return true;
                    else return false;
                });

                if ( count($user_subscriptions) ) {

                    uasort ($user_subscriptions, function($a, $b) {
                        if ( $a['end_date'] == $b['end_date'] ) return 0;
                        return ( $a['end_date'] > $b['end_date'] ) ? 1 : -1;
                    });

                    $subscription = end($user_subscriptions);
                    $sub_key = key($user_subscriptions);

                    if ( $subscription['status'] === "expired" && ( $subscription['product_id'] == $post['subscription_id'] || $subscription['variation_id'] == $post['subscription_id'] ) ) {

                        if ( $end = $subscription['end_date'] ) {

                            FollowUpEmailsNab::log( $end );
                            FollowUpEmailsNab::log( $from );
                            FollowUpEmailsNab::log( $to );

                            if ( strtotime($end) > strtotime($from) and strtotime($end) < strtotime($to) ) {

                                $recipient = (object) array(
                                    'user_id' => 0,
                                    'email'   => $user->user_email,
                                    'name'    => $first_name .' '. $last_name
                                );

                                if ($recipient = apply_filters('fue_nab_expired_subscriber_timeframe', $recipient, $sub_key, $subscription, $post, $user_id)) {
                                    $key = $recipient->user_id .'|'. $recipient->email .'|'. $recipient->name . '|' . $sub_key;
                                    $recipients[$key] = array($recipient->user_id, $recipient->email, $recipient->name, 'subscription_key' => $sub_key);
                                }

                            }

                        }

                    }

                }

            }

        }

        return $recipients;
    }

}

$GLOBALS['fue_nab_subscriptions'] = new FUE_NAB_Subscriptions();