<?php

class FUE_NAB_Admin {

    public function __construct() {
        add_filter('fue_email_defaults', array($this, 'filter_email_defaults'), 10, 2 );

        self::include_files();
    }

    public static function add_menu() {

        if (WP_ENV == 'development' || WP_ENV == 'dev' || WP_ENV == 'staging') {
            add_submenu_page( 'followup-emails', __('Subscriptions Debug', 'follow_up_emails_nab'), __('Subscriptions Debug', 'follow_up_emails_nab'), 'manage_follow_up_emails', 'followup-emails-subscriptions', 'FUE_NAB_Admin::subscriptions' );
        }
        
    }

    public static function include_files() {
        require_once FUE_NAB_DIR .'/templates/emails_list.php';
        require_once FUE_NAB_DIR .'/templates/send_manual.php';
        require_once FUE_NAB_DIR .'/templates/email-form/step2.php';
        require_once FUE_NAB_DIR .'/templates/email-form/step3.php';
    }

    public static function subscriptions() {
        global $wpdb;

        include FUE_NAB_TEMPLATES_DIR .'/subscriptions.php';    
    }


    /*public static function add_menu() {
        do_action( 'fue_menu' );
    }*/

    public static function filter_email_defaults($defaults, $id) {
        global $wpdb;

        if ( $id ) {

            $email    = $wpdb->get_row( $wpdb->prepare("SELECT * FROM {$wpdb->prefix}followup_emails WHERE id = %d", $id) );

            $defaults = array_merge($defaults, array(
                'subscription_recipient' => $email->subscription_recipient,
                'subscription_lengths'   => explode(',', $email->subscription_lengths),
                'not_renewed'            => $email->not_renewed,
                'to'                     => $email->send_to,
                'background'             => $email->background,
            ));

        } else {

            $defaults = array_merge($defaults, array(
                'subscription_recipient' => '',
                'subscription_lengths'   => array(1,3,6,12),
                'not_renewed'            => '0',
                'to'                     => '',
                'background'             => '',
            ));

        }

        return $defaults;
    }

}

$fue_nab_admin = new FUE_NAB_Admin;