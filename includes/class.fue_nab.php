<?php

//require_once 'class.fue_link_replacement.php';

class FUE_NAB {

    public function __construct() {

        add_filter( 'fue_nab_get_html_email_css', array($this, 'filter_get_html_email_css'), 10, 2 );

        add_filter( 'fue_nab_email_passes_filter', array($this, 'email_passes_filter'), 10, 2 );

        add_filter( 'fue_send_email_data', array($this, 'filter_send_to'), 25, 3 );

        add_filter( 'fue_nab_manual_email_to', array($this, 'filter_manual_email_to'), 10, 3);

        add_filter( 'fue_pre_save_data', array($this, 'pre_save_data'), 10, 2 );

        add_filter( 'fue_send_test_email_message', array($this, 'filter_send_test_email_message'), 10, 1 );

        //add_action( 'fue_email_created', array($this, 'email_created'), 10, 2 );

        add_filter( 'fue_manual_email_args', array($this, 'filter_manual_email_args'), 10, 2 );

        add_filter( 'fue_nab_active_subscriber', array($this, 'filter_active_subscriber'), 10, 5 );

        add_filter( 'fue_nab_expired_subscriber',           array($this, 'filter_expired_subscriber'), 10, 5 );
        add_filter( 'fue_nab_expired_subscriber_timeframe', array($this, 'filter_expired_subscriber'), 10, 5 );

        add_filter( 'fue_nab_send_manual_emails_args', array($this, 'filter_send_manual_emails_args'), 10, 1 );

    }

    public static function filter_get_html_email_css($innerCss, $background = null) {

        $backgroundable = ($background)
            ? '.backgroundable{background:#'. $background. '}'
            : '';

        $innerCss = $backgroundable . " " . $innerCss;

        return $innerCss;
    }

    public static function fail_email_filter($email_order = null, $options, $order, $subscription = null) {
        global $wpdb;

        if ( isset($email_order) ) {

            $wpdb->update(
                "{$wpdb->prefix}followup_email_orders",
                array('status' => 2),
                array('id' => $email_order->id)
            );

        }

        return false;
    }

    public static function email_filter($email_order = null, $options, $order, $subscription = null) {
        global $wpdb;

        $orderItems = $order->get_items();
        $orderItem = reset($orderItems);

        FollowUpEmailsNab::log("filter", true);

        if ( WC_Subscriptions::is_duplicate_site() ) {

            FollowUpEmailsNAB::log("email not sent: duplicate site");

            return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false

        }

        if ($options->subscription_recipient) {

            if ($options->subscription_recipient == 'themself') {

                //FollowUpEmailsNab::log($orderItem['Recipient']);

                //if ( $orderItem['Recipient'] != "For myself" and ! empty($orderItem['Recipient']) ) {
                if ( $orderItem['Recipient'] == "For a gift" ) {

                    FollowUpEmailsNab::log("not themself");

                    return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false
                }

            }

            if ($options->subscription_recipient == 'gift') {

                //FollowUpEmailsNab::log($orderItem['Recipient']);

                if ($orderItem['Recipient'] != "For a gift") {

                    FollowUpEmailsNab::log("not a gift");

                    return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false
                }

            }

        }

        if ($options->subscription_lengths) {

            $length = null;

            $lengths = explode(',', $options->subscription_lengths);


            if ( $variation_id = $orderItem['variation_id'] ) {
            
                $products = array(
                    1  => array(164, 165, 166),
                    3  => array(167, 168, 169),
                    6  => array(170, 171, 172),
                    12 => array(173, 174, 175)
                );

                foreach ($products as $l => $t) {
                    if (in_array($variation_id, $t)) {
                        $length = $l;
                    }
                }

            } else {

                if ( $orderItem['subscription_interval'] == 1 and $orderItem['subscription_period'] == 'month' ) {
                    $length = 1;
                }

                if ( $orderItem['subscription_interval'] == 3 and $orderItem['subscription_period'] == 'month' ) {
                    $length = 3;
                }

                if ( $orderItem['subscription_interval'] == 6 and $orderItem['subscription_period'] == 'month' ) {
                    $length = 6;
                }

                if ( $orderItem['subscription_interval'] == 1 and $orderItem['subscription_period'] == 'year' ) {
                    $length = 12;
                }

            }
            

            if ( ! in_array($length, $lengths)) {

                FollowUpEmailsNab::log("not the right length");
                FollowUpEmailsNab::log($variation_id);
                FollowUpEmailsNab::log($length);
                FollowUpEmailsNab::log(implode(',', $lengths));

                return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false
            }

        }

        if ($options->not_renewed) {

            //FollowUpEmailsNab::log(1, true);

                //FollowUpEmailsNab::log(print_r($options, true));


            if ( $options->interval_type == "subs_expired" ) { // $subscription not available

                FollowUpEmailsNab::log("-{$options->interval_num} {$options->interval_duration}");

                $expired_at = strtotime("-{$options->interval_num} {$options->interval_duration}");

                $user_id = $order->get_user()->ID;

                FollowUpEmailsNab::log($user_id, true);

                FollowUpEmailsNab::log( date('Y-m-d H:i:s', $expired_at), true);

                $args = array(
                    'numberposts' => -1,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => $user_id,
                    'post_type'   => 'shop_order',
                    'post_status' => array('wc-processing', 'wc-completed'),
                    'date_query'  => array(
                        array(
                            'after' => date('Y-m-d H:i:s', $expired_at),
                        )
                    )
                );
             
                $orders = new WP_Query($args);

                //FollowUpEmailsNab::log( print_r( $orders, true) );

                if ($orders->have_posts()) :
                    while ($orders->have_posts()) : $orders->the_post();
                 
                        FollowUpEmailsNab::log("customer has orders since expiry");
                        return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false
                 
                    endwhile;
                endif;

            }
            

            if ( $options->send_type == "expired_subscription" or $options->send_type == "expired_subscription_timeframe" ) {

                FollowUpEmailsNab::log( print_r( $subscription, true) );

                $end_date = $subscription['end_date'];


                $user_id = $order->get_user()->ID;

                FollowUpEmailsNab::log($user_id, true);

                FollowUpEmailsNab::log( $end_date, true);

                $args = array(
                    'numberposts' => -1,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => $user_id,
                    'post_type'   => 'shop_order',
                    'post_status' => array('wc-processing', 'wc-completed'),
                    'date_query'  => array(
                        array(
                            'after' => $end_date,
                        )
                    )
                );
             
                $orders = new WP_Query($args);


                if ($orders->have_posts()) :
                    while ($orders->have_posts()) : $orders->the_post();

                        FollowUpEmailsNab::log( print_r( $orders, true) );

                        FollowUpEmailsNab::log("customer has orders since expiry");
                        return self::fail_email_filter($email_order, $options, $order, $subscription); // returns false
                 
                    endwhile;
                endif;

            }

        }


        FollowUpEmailsNab::log("filter OK");

        return $success = true;

    }

    public static function email_passes_filter($success, $email_order) {
        global $wpdb;

        $email = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_emails` WHERE `id` = '%d'", $email_order->email_id) );

        if ($email->email_type === "manual") { // email has already been filtered, return true
            FollowUpEmailsNab::log('filter: manual');
            return true;
        }

        $order = new WC_Order($email_order->order_id);

        return self::email_filter($email_order, $email, $order);

    }


    public static function send_to($email_to, $orderItem, $send_to) {

        FollowUpEmailsNab::log($send_to);

        if ($send_to === 'recipient-or-buyer') {
            
            // use the recipient email address if available, otherwise send to the buyer
            if ( ! empty($orderItem['Recipient Email Address - Text'])) {
                FollowUpEmailsNab::log('r' - $orderItem['Recipient Email Address - Text']);
                $email_to = $orderItem['Recipient Email Address - Text'];
            }

        } else if ($send_to === 'recipient-only-or-buyer') {

            // use the recipient email address if gift, send to no one if blank, otherwise leave as buyer email
            if ($orderItem['Recipient'] == "For a gift") {
                $email_to = $orderItem['Recipient Email Address - Text'];
            }

        } else if ($send_to === 'recipient') {

            // use the recipient email address if available, otherwise send to buyer
            if ( ! empty($orderItem['Recipient Email Address - Text'])) {
                $email_to = $orderItem['Recipient Email Address - Text'];
            }
        
        } else if ($send_to === 'recipient-only') {
            
            // use the recipient email address if available, otherwise send to no one
            $email_to = ($orderItem['Recipient Email Address - Text']) ?: '';
        
        } else if ($send_to === 'webmaster@notanotherbill.com') {

            $email_to = 'webmaster@notanotherbill.com';

        } else if ($send_to === 'nab@robertsandiford.co.uk') {

            $email_to = 'nab@robertsandiford.co.uk';

        }

        FollowUpEmailsNab::log($email_to);

        return $email_to;

    }

    public static function filter_send_to($email_data, $email_order, $email) {

        $order = new WC_Order($email_order->order_id);
        $orderItems = $order->get_items();
        $orderItem = reset($orderItems);

        FollowUpEmailsNab::log('filter send to');

        if ( $email_order->user_id == 0 && $email->email_type === 'manual' ) { // set manual emails back to their rightful recipient
            $email_data['email_to'] = $email_data['meta']['email_address'];

            return $email_data;
        }


        /*if ($email->email_type === "manual") { // send to has already been filtered, return original data
            FollowUpEmailsNab::log('send to: manual');
            return $email_data;
        }*/

        FollowUpEmailsNab::log($email_data['email_to']);

        $email_data['email_to'] = self::send_to($email_data['email_to'], $orderItem, $email->send_to);

        FollowUpEmailsNab::log($email_data['email_to']);

        return $email_data;

    }

    /*public static function email_created($id, $data) {
        global $wpdb;

        FollowUpEmailsNab::log("email-created");

        die('email-created');

        $email = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_emails` WHERE `id` = '%d'", $id) );

        populate_existing_subscriptions($email);
    }*/

    public static function pre_save_data($data, $post) {

        $step = absint( $post['step'] );

        if ( $step == 2 ) {

            $data['send_to']                 = $post['send_to'];

            $data['subscription_recipient']  = $post['subscription_recipient'];

            $data['subscription_lengths']    = implode(',', array_filter(array(
                (! empty($post['subscription_length_1month']))  ? '1'  : '',
                (! empty($post['subscription_length_3month']))  ? '3'  : '',
                (! empty($post['subscription_length_6month']))  ? '6'  : '',
                (! empty($post['subscription_length_12month'])) ? '12' : '',
            )));

            $data['not_renewed']             = $post['not_renewed'];

        } else if ( $step == 3 ) {

            $data['background']              = $post['background'];

            $data['message']                 = str_replace('src="https://', 'src="http://', $data['message']);

        }

        return $data;

    }

    public static function filter_send_test_email_message($message) {

        $message = str_replace('src="https://', 'src="http://', $message);

        return $message;

    }

    public static function filter_manual_email_args($args, $post) {

        $args + array(
            'send_to'                => $post['send_to'],
            'subscription_recipient' => $post['subscription_recipient'],
            'subscription_lengths'   => implode(',', array_filter(array(
                                            (! empty($post['subscription_length_1month']))  ? '1'  : '',
                                            (! empty($post['subscription_length_3month']))  ? '3'  : '',
                                            (! empty($post['subscription_length_6month']))  ? '6'  : '',
                                            (! empty($post['subscription_length_12month'])) ? '12' : '',
                                        ))),
            'not_renewed'            => $post['not_renewed']
        );

        return $args;
    }

    public static function filter_active_subscriber($recipient, $sub_key, $subscription, $post, $user_id) {

        FollowUpEmailsNab::log('filter_active_subscriber');
        

        // get the order item
        $order_id = $subscription['order_id'];
        $order = new WC_Order($order_id);
        $orderItems = $order->get_items();
        $orderItem = reset($orderItems);


        FollowUpEmailsNab::log( 'start date' );
        FollowUpEmailsNab::log( $orderItem['Start Date - Date'] );
        FollowUpEmailsNab::log( $orderItem['subscription_start_date'] );
        FollowUpEmailsNab::log( strtotime($orderItem['Start Date - Date']) );
        FollowUpEmailsNab::log( time() );


        // filter out invalid subscriptions 
        if ( ! ( $order->post_status === 'wc-processing' or $order->post_status === 'wc-completed' ) ) { // paid for
            FollowUpEmailsNab::log('subscriber filtered: unpaid');
            return false;
        }

        if ( ! ( time() > strtotime($orderItem['Start Date - Date']) ) ) { // current date is after start date
            FollowUpEmailsNab::log('subscriber filtered: not started yet');
            return false;
        }

        ## debug
        if ( ! ( $orderItem['subscription_interval'] > 0 ) ) { // valid subscription interval (# months)
            FollowUpEmailsNab::log('subscriber filtered: invalid subscription interval');
            return false;
        }

        if ( ! ( 
            $orderItem['subscription_interval'] == 1                      // rolling subscription
            or (
                time() < strtotime($orderItem['subscription_expiry_date'])
                and
                $orderItem['subscription_expiry_date'] != 0
            ) // or current date is before expiry data
        ) ) {
            FollowUpEmailsNab::log('subscriber filtered: has expired');
            return false;
        }

        /*if ( ! (
            ( $order->post_status === 'wc-processing' or $order->post_status === 'wc-completed' ) // paid for
            and ( time() > strtotime(wc_get_order_item_meta($orderItem, 'Start Date - Date')) )   // current date is after start date
            and ( 
                wc_get_order_item_meta($orderItem, '_subscription_interval') == 1                      // rolling subscription
                or time() < strtotime(wc_get_order_item_meta($orderItem, '_subscription_expiry_date')) // or current date is before expiry data
            )
        ) ) {
            return false;
        }*/


        return self::filter_subscriber($recipient, $sub_key, $subscription, $post, $user_id, $order, $orderItem);

    }

    public static function filter_expired_subscriber($recipient, $sub_key, $subscription, $post, $user_id) {

        FollowUpEmailsNab::log('filter_expired_subscriber');

        // get the order item
        $order_id = $subscription['order_id'];
        $order = new WC_Order($order_id);
        $orderItems = $order->get_items();
        $orderItem = reset($orderItems);


        return self::filter_subscriber($recipient, $sub_key, $subscription, $post, $user_id, $order, $orderItem);

    }

    public static function filter_subscriber($recipient, $sub_key, $subscription, $post, $user_id, $order, $orderItem) {

        FollowUpEmailsNab::log('filter_subscriber');
        

        // get the options from post
        /*$options = (object) array(
            'send_type'              
            'subscription_recipient' => $post['subscription_recipient'],
            'subscription_lengths'   => implode(',', array_filter(array(
                                            (! empty($post['subscription_length_1month']))  ? '1'  : '',
                                            (! empty($post['subscription_length_3month']))  ? '3'  : '',
                                            (! empty($post['subscription_length_6month']))  ? '6'  : '',
                                            (! empty($post['subscription_length_12month'])) ? '12' : '',
                                        ))),
            'not_renewed'            => $post['not_renewed']
        );*/

        $options = (object) $post;

        $options->subscription_lengths = implode(',', array_filter(array(
            (! empty($options->subscription_length_1month))  ? '1'  : '',
            (! empty($options->subscription_length_3month))  ? '3'  : '',
            (! empty($options->subscription_length_6month))  ? '6'  : '',
            (! empty($options->subscription_length_12month)) ? '12' : '',
        )));


        if ($passes = self::email_filter(null, $options, $order, $subscription)) {
            
            $user = new WP_User($user_id);
            $billing_first_name = get_user_meta( $user_id, 'billing_first_name', true );
            $billing_last_name  = get_user_meta( $user_id, 'billing_last_name', true );

            if ($billing_first_name) {
                $name = trim( $billing_first_name . ' ' . $billing_last_name );
            } else if ($user->first_name) {
                $name = trim( $user->first_name . ' ' . $user->last_name );
            } else {
                $user_info = get_userdata($user_id);
                $name = $user_info->data->display_name;
            }

            $recipient->name = $name;

            $recipient->email = apply_filters('fue_nab_manual_email_to', $recipient->email, $orderItem, $post['send_to']);

            return $recipient;
        }

        return false;
 
    }

    public static function filter_manual_email_to($email_to, $orderItem, $send_to) {

        return $email_to = self::send_to($email_to, $orderItem, $send_to);

    }

    public static function filter_send_manual_emails_args ($args) {
        
        $args['message'] = str_replace('src="https://', 'src="http://', $args['message']);

        return $args;

    }

}

$fue_nab = new FUE_NAB;