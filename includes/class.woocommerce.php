<?php

class FUE_NAB_Woocommerce {

    public function __construct() {
        global $wpdb, $woocommerce;

        self::load_addons();
    }

    public function load_addons() {
        require_once FUE_NAB_INC_DIR .'/class.fue_nab_subscriptions.php';
    }

}

$GLOBALS['fue_nab_woocommerce'] = new FUE_NAB_Woocommerce();