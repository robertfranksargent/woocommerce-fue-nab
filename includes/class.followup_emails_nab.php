<?php

class FollowUpEmailsNab {

    public static $db_version           = '6.1';
    public static $is_woocommerce       = false;
    public static $is_sensei            = false;

    public function __construct() {

        self::include_files();

        if ( self::is_woocommerce_installed() ) {
            self::$is_woocommerce = true;
            require_once FUE_NAB_INC_DIR .'/class.woocommerce.php';
        }

    }

    public static function include_files() {

        require_once FUE_NAB_INC_DIR .'/class.fue_nab.php';
        require_once FUE_NAB_INC_DIR .'/plugin_hooks.php';

        if ( is_admin() ) {
            require_once FUE_NAB_INC_DIR .'/class.fue_nab_admin.php';
        }

    }

    /*public static function include_files() {
        require_once FUE_INC_DIR .'/class.fue_compat.php';
    }*/

    public static function is_woocommerce_installed() {
        return in_array('woocommerce/woocommerce.php', get_option('active_plugins'));
    }

    public static function is_sensei_installed() {
        return in_array('woothemes-sensei/woothemes-sensei.php', get_option('active_plugins'));
    }

    public static function is_follow_up_emails_installed() {
        return in_array('woocommerce-follow-up-emails/woocommerce-follow-up-emails.php', get_option('active_plugins'));
    }

    /**
     * Check is the installed version of WooCommerce is 2.1 or newer
     */
    public static function is_woocommerce_pre_2_1() {
        return !function_exists('wc_add_notice');
    }

    /*public static function load_addons() {
        do_action( 'fue_addons_loaded' );
    }*/

    public static function update_db( $force = false ) {
        global $wpdb;

        //$installed_db_version = get_option('fue_nab_db_version', 0);

        //if ( !$force && $installed_db_version == self::$db_version ) return;

        $wpdb->hide_errors();
        $collate = '';

        if ( method_exists($wpdb, 'has_cap') ) {
            if ( $wpdb->has_cap('collation') ) {
                if( ! empty($wpdb->charset ) ) $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
                if( ! empty($wpdb->collate ) ) $collate .= " COLLATE $wpdb->collate";
            }
        } else {
            if ( $wpdb->supports_collation() ) {
                if( ! empty($wpdb->charset ) ) $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
                if( ! empty($wpdb->collate ) ) $collate .= " COLLATE $wpdb->collate";
            }
        }

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table = $wpdb->prefix . 'followup_emails';
        $sql = "CREATE TABLE $table (
id bigint(20) NOT NULL AUTO_INCREMENT,
product_id bigint(20) NOT NULL,
category_id BIGINT(20) NOT NULL DEFAULT 0,
name varchar(100) NOT NULL,
interval_num int(3) DEFAULT 1 NOT NULL,
interval_duration VARCHAR(50) DEFAULT 'days' NOT NULL,
interval_type VARCHAR(50) DEFAULT  'purchase' NOT NULL,
send_date VARCHAR(50) DEFAULT '' NOT NULL,
send_date_hour VARCHAR(4) DEFAULT '' NOT NULL,
send_date_minute VARCHAR(4) DEFAULT '' NOT NULL,
subject VARCHAR(255) NOT NULL,
message LONGTEXT NOT NULL,
tracking_code VARCHAR(255) NOT NULL,
usage_count BIGINT(20) DEFAULT 0 NOT NULL,
date_added DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
email_type VARCHAR(50) DEFAULT '' NOT NULL,
priority INT(3) DEFAULT 0 NOT NULL,
always_send INT(1) DEFAULT 0 NOT NULL,
meta TEXT NOT NULL,
status INT(1) DEFAULT 1 NOT NULL,
send_to ENUM('buyer', 'recipient-or-buyer', 'recipient-only-or-buyer', 'recipient', 'recipient-only', 'webmaster@notanotherbill.com', 'nab@robertsandiford.co.uk') DEFAULT 'buyer' NOT NULL,
subscription_recipient ENUM('', 'themself', 'gift') DEFAULT '' NOT NULL,
subscription_lengths VARCHAR(8) DEFAULT '1,3,6,12' NOT NULL,
not_renewed BOOL DEFAULT 0 NOT NULL,
background VARCHAR(6) DEFAULT '' NOT NULL,
KEY category_id (category_id),
KEY product_id (product_id),
KEY email_type (email_type),
KEY status (status),
PRIMARY KEY  (id)
) $collate";
        dbDelta($sql);

        $table = $wpdb->prefix .'followup_failed_payments';
        $sql = "CREATE TABLE $table (
id INT NOT NULL AUTO_INCREMENT,
subscription_id VARCHAR(30) NOT NULL,
datetime DATETIME NOT NULL,
times TINYINT DEFAULT 0 NOT NULL,
PRIMARY KEY  (id)
) $collate";
        dbDelta($sql);
    }

    public static function load_addons() {

        if ( self::$is_woocommerce ) {
            require_once FUE_NAB_INC_DIR .'/class.woocommerce.php';
        }

    }

    public static function install() {

        // check for existing plugin
        if (self::is_follow_up_emails_installed()) {

            // install db tables
            self::update_db();

        } else {

            // deactivate
            // show error message
            trigger_error('Unable to activate Follow Up Emails NAB because Follow Up Emails is not active', E_USER_ERROR);

        }

    }

    public static function uninstall() {
        // delete database tables
        //self::delete_tables();
    }

    public static function delete_tables() {
        global $wpdb;

        $table = $wpdb->prefix . 'followup_emails';
        $sql = "ALTER TABLE $table
                DROP COLUMN `to`,
                DROP COLUMN `subscription_recipient`,
                DROP COLUMN `subscription_lengths`";
        dbDelta($sql);
    }

    public static function ajax_apply_to_existing_subscriptions() {
        global $wpdb;

        $id = $_POST['id'];

        if ( ! $email = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_emails` WHERE `id` = '%d'", $id) ) ) {

            $resp = array(
                'status'    => 'ERROR',
                'message'   => 'Email not found'
            );

            die(json_encode($resp));

        }

        $count = self::populate_existing_subscriptions($email);

        $resp = array(
            'status'    => 'OK',
            'message'   => "emails scheduled for {$count} subscriptions"
        );

        die(json_encode($resp));
    }

    public static function populate_existing_subscriptions($email) {
        global $wpdb;

        FollowUpEmailsNab::log("populate existing");

        $email_id = $email->id;
        $usersSubscriptions = WC_Subscriptions_Manager::get_all_users_subscriptions();
        $count = 0;

        foreach ($usersSubscriptions as $user_id => $subscriptions) {

            $user = new WP_User($user_id);

            foreach ($subscriptions as $subscription_key => $subscription) {

                if ( $expiry_date = strtotime($subscription['expiry_date']) ) {


                    $interval_num = $email->interval_num;

                    $interval_duration = substr($email->interval_duration, 0, -1); // remove trailing 's'


                    $send_on = 0;

                    switch( $email->interval_type ) {

                        case 'subs_before_expire':
                            if ( $subscription['status'] == 'active' ) {
                                $send_on = strtotime("-{$interval_num} {$interval_duration}", $expiry_date);
                            }
                            break;

                    }

                    if ( $send_on > time() ) {

                        $order_id = $subscription['order_id'];
                        $product_id = $subscription['product_id'];

                        if ( $order_id and $product_id ) {

                            $existing_orders = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM `{$wpdb->prefix}followup_email_orders` WHERE `order_id` = '%d' and `email_id` = '%d'", $order_id, $email->id) );

                            if ($existing_orders === "0") {

                                $insert = array(
                                    'user_id'         => $user_id,
                                    'user_email'      => $user->user_email,
                                    'order_id'        => $order_id,
                                    'product_id'      => 0,
                                    'email_id'        => $email_id,
                                    'send_on'         => $send_on,
                                    'is_cart'         => 0,
                                    'is_sent'         => 0,
                                    'date_sent'       => '0000-00-00 00:00:00',
                                    'email_trigger'   => '',
                                    'meta'            => serialize(array("subs_key" => $subscription_key)),
                                    'status'          => 1
                                );

                                if ($res = $wpdb->insert("{$wpdb->prefix}followup_email_orders", $insert)) {
                                    $count++;
                                }

                            }

                        }

                    }

                }

            }
        }
        return $count;
    }

    function populate_all_existing_subscriptions() {
        global $wpdb;

        $emails = $wpdb->get_results( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}followup_emails` WHERE `interval_type` = 'subs_before_expire'") );
        foreach ($emails as $email) {
            populate_existing_subscriptions($email);
        }
    }

    public static function log($text) {
        $root_path = FUE_NAB_DIR;
        // file_put_contents($root_path . '/log.txt', date('m/d/Y H:i:s ', time()) . $text . "\r\n", FILE_APPEND);
    }

}