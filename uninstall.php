<?php

if( !defined( 'ABSPATH') && !defined('WP_UNINSTALL_PLUGIN') ) {
    exit;
}
global $wpdb;

delete_option('woocommerce_followup_nab_db_version');