NAB automated emails for woocommerce w/ subscriptions

--notes 


Successful Subscription Order
Recurring Payment Fails
Recurring Payment Doesn't Set Back Up. (Can't collect the months money)
Customer Puts Recurring Subscription On Hold
Customer Cancels Recurring Subscription (That is not for themselves)
Customer Cancels Recurring Subscription That Is For A Gift (Not themselves)
Subscription Coming To End (3,6 & 12 Month only)
Subscription Come To End (3/6/12 Month) (if I purchased for myself)
Subscription Finished (buyer) (3/6/12 Month) (if I purchased for gift)
Subscription Finished (recipient) (3/6/12 Month) (if I purchased for gift)
If They Have Dropped Off & Not Renewed (All 3/6/12 & rollers)
If They Have Dropped Off & Not Renewed (All 3/6/12 & rollers)
Monthly Subscription Dispatched Email (buyer)
Monthly Subscription Dispateched Email (recipient)
If A Customer Changes Their Preferences Or Delivery Address
2 Months into a subscription
Shop Confirmation Email
Shop Dispatched Email


triggers:

get last dispatch date from expirey date
use das from/before last dispatch date

first dispatch after expiry

first dispatch after expiry

subscription purchased **
subscription on hold **
cancels subscription **
subscription coming to an end (5 days before last dispatch) ++
subscription finished (10 days after last dispatch) ++
subscription expired and not renewed (5 days before first dispatch)
subscription expired and not renewed (10 days after first dispatch)
subscription item dispatched
payment fails (once) **
payment fails (3 times)
change of prefs
shop item purchased
shop item dispatched

conditions:

for
  for themself
  as a gift

subscription length
  3 months
  6 months
  12 months
  rolling


Subscription Products
164 monthly 24
165 monthly 28
166 monthly 32
167 3 month 69
168 3 month 80
169 3 month 95
170 6 month 129
171 6 month 155
172 6 month 180
173 12 month 229
174 12 month 290
175 12 month 340

orders@notanotherbill.com


====================================

<?php 

add_action( 'init', array($this, 'hook_statuses') );
add_action( 'woocommerce_checkout_order_processed', array($this, 'order_status_updated') );

order_status_updated

create_email_orders

function hook_statuses() {

        if ( WC_NAB_AE_Compatibility::is_wc_version_gt('2.2') ) {
            $order_statuses = wc_get_order_statuses();

            foreach ( $order_statuses as $key => $status ) {
                add_action('woocommerce_order_status_'. str_replace( 'wc-', '', $key ), array($this, 'order_status_updated'), 100);
            }
        } else {
            $order_statuses = (array)get_terms( 'shop_order_status', array('hide_empty' => 0, 'orderby' => 'id') );

            if (! isset($order_statuses['errors']) ) {
                foreach ( $order_statuses as $status ) {
                    add_action('woocommerce_order_status_'. $status->slug, array($this, 'order_status_updated'), 100);
                }
            }
        }

    }


====================================


add_action( 'activated_subscription', array($this, 'activated_subscription') );

add_action( 'cancelled_subscription', array($this, 'cancelled_subscription') );

add_action( 'subscription_expired', array($this, 'subscription_expired') );

add_action( 'subscription_put_on-hold', array($this, 'subscription_put_on_hold') );

add_action( 'processed_subscription_payment_failure', array($this, 'processed_subscription_payment_failure') );

add_action( 'updated_users_subscriptions', array($this, 'updated_users_subscriptions') );


function activated_subscription($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    //self::record_order( $order );

    //if ( $order_status == 'processing' || $order_status == 'completed' ) {

        // only queue date and customer emails once per order
        ##if ( get_post_meta( $subscription_key, '_order_status_emails_queued', true ) != true ) {
            
            $triggers[] = 'subscription_purchased';

            ##update_post_meta( $order_id, '_order_status_emails_queued', true );
        ##}

    }


    // $triggers = apply_filters( 'nab_ae_new_order_triggers', $triggers, $subscription_key );

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

function cancelled_subscription($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    $triggers[] = 'cancelled_subscription';

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

function subscription_expired($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    $triggers[] = 'subscription_expired';

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

function subscription_put_on_hold($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    $triggers[] = 'subscription_put_on_hold';

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

function processed_subscription_payment_failure($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    $triggers[] = 'processed_subscription_payment_failure';

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

function updated_users_subscriptions($user_id, $subscription_key) {
    global $wpdb;

    $subscription = WC_Subscriptions_Manager::get_subscription($subscription_key);

    $triggers     = array();

    $triggers[] = 'updated_users_subscriptions';

    self::create_email_subscription_orders( $triggers, $subscription_key );

}

public static function create_email_subscription_orders( $triggers, $subscription_key = '' ) {
    global $woocommerce, $wpdb;

    $subscription   = WC_Subscriptions_Manager::get_subscription($subscription_key) ?: false;
    /*$items          = $order->get_items();
    $item_ids       = array();
    $queued         = array();
    $order_created  = false;*/

    /*if ( $subscription ) {

        if ( WC_NAB_AE_Compatibility::get_order_user_id( $order ) > 0 ) {
            $nab_ae_customer_id = $wpdb->get_var( $wpdb->prepare("SELECT id FROM {$wpdb->prefix}nab_ae_customers WHERE user_id = %d", WC_NAB_AE_Compatibility::get_order_user_id( $order ) ) );
        } else {
            $nab_ae_customer_id = $wpdb->get_var( $wpdb->prepare("SELECT id FROM {$wpdb->prefix}nab_ae_customers WHERE email_address = %s", $order->billing_email) );
        }

    }*/

    $trigger = '';
    foreach ( $triggers as $t ) {
        $trigger .= "'". esc_sql($t) ."',";
    }
    $trigger = rtrim($trigger, ',');

    if ( empty($trigger) ) $trigger = "''";


    // find a product match
    $emails         = array();
    //$always_prods   = array();
    //$always_cats    = array();

    /*foreach ( $items as $item ) {
        $prod_id = (isset($item['id'])) ? $item['id'] : $item['product_id'];

        if (! in_array($prod_id, $item_ids) )
            $item_ids[] = $prod_id;

        // variation support
        $parent_id = -1;
        if ( isset($item['variation_id']) && $item['variation_id'] > 0 ) {
            $parent_id  = $prod_id;
            $prod_id    = $item['variation_id'];
        }*/

        $email_results = $wpdb->get_results( $wpdb->prepare("
            SELECT DISTINCT `id`, `product_id`, `priority`, `meta`, `interval_type`
            FROM {$wpdb->prefix}nab_ae_emails
            WHERE `interval_type` IN ($trigger)
            AND `email_type` <> 'generic'
            AND `always_send` = 0
            AND status = ". NAB_AE::STATUS_ACTIVE ."
            ORDER BY `priority` ASC
        ") );

        if ( $email_results ) {
            foreach ($email_results as $email) {
                $meta = maybe_unserialize($email->meta);

                // skip date emails that have passed
                //if ( $email->interval_type == 'date' && NAB_AE::send_date_passed( $email->id ) ) {
                //    continue;
                //}

                //if ( $prod_id == $email->product_id ) {
                    // exact product ID match
                    $emails[] = array('id' => $email->id, 'item' => $prod_id, 'priority' => $email->priority);
                //} elseif ( $parent_id > 0 && $parent_id == $email->product_id && isset($meta['include_variations']) && $meta['include_variations'] == 'yes' ) {
                //    $emails[] = array('id' => $email->id, 'item' => $parent_id, 'priority' => $email->priority);
                //}
            }
        }

        // always_send product matches
        /*$results = $wpdb->get_results( $wpdb->prepare("
            SELECT DISTINCT `id`, `product_id`, `meta` , `email_type`, `interval_type`
            FROM {$wpdb->prefix}nab_ae_emails
            WHERE `interval_type` IN ($trigger)
            AND ( `product_id` = %d OR `product_id` = %d OR `email_type` = 'reminder' )
            AND `always_send` = 1
            AND status = ". NAB_AE::STATUS_ACTIVE ."
        ", $prod_id, $parent_id) );

        foreach ( $results as $row ) {
            $meta = maybe_unserialize($row->meta);

            // skip date emails that have passed
            if ( $row->interval_type == 'date' && NAB_AE::send_date_passed( $row->id ) ) {
                continue;
            }

            if ( $row->email_type == 'reminder' ) {
                $always_prods[] = array( 'id' => $row->id, 'item' => $prod_id, 'parent' => $parent_id );
            } elseif ( $prod_id == $row->product_id ) {
                // exact product ID match
                $always_prods[] = array( 'id' => $row->id, 'item' => $prod_id );
            } elseif ( $parent_id > 0 && $parent_id == $row->product_id && isset($meta['include_variations']) && $meta['include_variations'] == 'yes' ) {
                $always_prods[] = array( 'id' => $row->id, 'item' => $parent_id );
            }
        }*/

        // always_send category matches
        /*if ( $parent_id > 0 )
            $cat_ids    = wp_get_object_terms( $parent_id, 'product_cat', array('fields' => 'ids') );
        else
            $cat_ids    = wp_get_object_terms( $prod_id, 'product_cat', array('fields' => 'ids') );

        $ids        = implode(',', $cat_ids);

        if (empty($ids)) $ids = "''";

        $all_categories = array_merge($all_categories, $cat_ids);

        $results = $wpdb->get_results("
            SELECT DISTINCT `id`, `interval_type`
            FROM {$wpdb->prefix}nab_ae_emails
            WHERE `interval_type` IN ($trigger)
            AND `always_send` = 1
            AND status = ". NAB_AE::STATUS_ACTIVE ."
            AND ( `category_id` <> '' AND `category_id` IN (". $ids .") )
        ");

        foreach ( $results as $row ) {

            // skip date emails that have passed
            if ( $row->interval_type == 'date' && NAB_AE::send_date_passed( $row->id ) ) {
                continue;
            }

            $always_cats[] = array('id' => $row->id, 'item' => $prod_id);
        }*/

    }

}


scheduled_subscription_expiration



Action: 'activated_subscription'

Parameters:
$user_id Integer The ID of the user for whom the subscription was activated.
$subscription_key String The key for the subscription that was just set as activated on the user’s account.


Action: 'cancelled_subscription'
Parameters:
$user_id Integer The ID of the user for whom the subscription was cancelled.
$subscription_key String The key for the subscription that was just cancelled on the user’s account.

Description: Triggered when a subscription has been cancelled on a user’s account, which can be triggered by either an administrator, subscriber on their account page or the payment gateway.



Action: 'subscription_end_of_prepaid_term'
Parameters:
$user_id Integer The ID of the user for whom the subscription was cancelled.
$subscription_key String The key for the subscription that was just cancelled on the user’s account.

Description: Triggered when a subscription that was previously cancelled reaches the end of its prepaid term. For example, if a customer purchases a monthly subscription on the 1st of March, then cancels the subscription on the 15th of March, the 'cancelled_subscription' action will be triggered immediately and then the 'subscription_end_of_prepaid_term' action will be triggered on the 1st of April. If you are offering a virtual service, such as a membership, you should use this hook to cease providing that service, rather than the 'cancelled_subscription' hook. Subscriptions uses the 'subscription_end_of_prepaid_term' hook internally to change the user’s role for cancelled subscriptions.



Action: 'subscription_expired'

Parameters:
$user_id Integer The ID of the user for whom the subscription expired.
$subscription_key String The key for the subscription that just expired on the user’s account.

Description: Triggered when a subscription reaches the end of its term, if a length was set on the subscription when it was purchased. This event may be triggered by either WooCommerce Subscriptions, which schedules a cron-job to expire each subscription, or by the payment gateway extension which can call the WC_Subscriptions_Manager::expire_subscription() function directly.



Action: 'subscription_put_on-hold'

Parameters:
$user_id Integer The ID of the user who owns the subscription.
$subscription_key String The key for the subscription that has been put on hold.

Description: Triggered when a subscription is put on-hold (suspended). A subscription is put on hold when:

the store manager has manually suspended the subscription
the customer has manually suspended the subscription from her My Account page
a renewal payment is due (subscriptions are suspended temporarily for automatic renewal payments until the payment is processed successfully and indefinitely for manual renewal payments until the customer logs in to the store to pay for the renewal. For more information, see the subscription renewal guide).



Action: 'processed_subscription_payment_failure'

Parameters:
$user_id Integer The ID of the user who owns the subscription.
$subscription_key String The key for the subscription to which the failed payment relates.

Description: Triggered when a subscription payment is attempted for a subscription specified with $subscription_key but the payment failed.



Action: 'updated_users_subscriptions'

Parameters:
$user_id Integer The ID of the user for whom the subscriptions have been updated.
$subscription_details Array An associative array of arrays with a subscription key and corresponding ‘detail’ => ‘value’ pairs based on the subscription data structure.

Description: Triggered whenever the details, like expiry date or status, for a user’s subscription have been updated. $subscription_details includes the details that were updated or added to the user’s subscriptions.


<?php WC_Subscriptions_Manager::get_subscription( $subscription_key ) ?>