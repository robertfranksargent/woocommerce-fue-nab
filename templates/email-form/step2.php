<?php

class Step2 {

    public function __construct() {
        add_action('fue_nab_email_form_after_subject', array($this, 'email_form_after_subject'), 10, 1 );
        add_action('fue_email_form_interval_meta', array($this, 'email_form_interval_meta'), 10, 1 );

        add_filter('fue_nab_interval_duration_min', array($this, 'interval_duration_min'), 10, 1 );
    }

    public function email_form_after_subject($defaults) {
        if ($defaults['type'] === 'subscription') { ?>
            <div class="field">
                <label for="send_to"><?php _e('Who should the email be sent to?', 'follow_up_emails_nab'); ?></label>
                <select name="send_to" id="send_to">
                    <option value="buyer" <?php selected( $defaults['to'], 'buyer' ); ?>>Buyer</option>
                    <option value="recipient-or-buyer" <?php selected( $defaults['to'], 'recipient-or-buyer' ); ?>>Gift Recipient, Buyer if none</option>
                    <option value="recipient-only-or-buyer" <?php selected( $defaults['to'], 'recipient-only-or-buyer' ); ?>>Gift Recipient only</option>
                    <!--
                    <option value="recipient-or-buyer" <?php selected( $defaults['to'], 'recipient-or-buyer' ); ?>>Gift Recipient if gift, Buyer if empty. Buyer if for themself</option>
                    <option value="recipient-only-or-buyer" <?php selected( $defaults['to'], 'recipient-only-or-buyer' ); ?>>Gift Recipient if gift only. Buyer if for themself</option>
                    <option value="recipient" <?php selected( $defaults['to'], 'recipient' ); ?>>Gift Recipient if gift, Buyer if empty</option>
                    <option value="recipient-only" <?php selected( $defaults['to'], 'recipient-only' ); ?>>Gift Recipient if gift only</option>
                    -->
                </select>
            </div>
        <?php }
    }

    public function email_form_interval_meta($defaults) {
        if ($defaults['type'] === 'subscription') { ?>
            <div class="field">
                <label for="subscription_recipient">
                    <?php _e('Subscription purchased for:', 'follow_up_emails_nab'); ?>
                </label>
                <select name="subscription_recipient" id="subscription_recipient">
                    <option value="" <?php selected( $defaults['subscription_recipient'], '' ); ?>>All</option>
                    <option value="themself" <?php selected( $defaults['subscription_recipient'], 'themself' ); ?>>Themself</option>
                    <option value="gift" <?php selected( $defaults['subscription_recipient'], 'gift' ); ?>>As a gift</option>
                </select>
            </div>

            <div class="field">
                <label>
                    <?php _e('Subscription length:', 'follow_up_emails_nab'); ?>
                </label>
                <label for="subscription_length_1month">
                    <input type="checkbox" name="subscription_length_1month" id="subscription_length_1month" value="1" <?php if (in_array(1, $defaults['subscription_lengths'])) echo 'checked="checked"'; ?> />
                    1 Month Rolling
                </label>
                <label for="subscription_length_3month">
                    <input type="checkbox" name="subscription_length_3month" id="subscription_length_3month" value="1"
                        <?php if (in_array(3, $defaults['subscription_lengths'])) echo 'checked="checked"'; ?> />
                    3 Month
                </label>
                <label for="subscription_length_6month">
                    <input type="checkbox" name="subscription_length_6month" id="subscription_length_6month" value="1"
                        <?php if (in_array(6, $defaults['subscription_lengths'])) echo 'checked="checked"'; ?> />
                    6 Month
                </label>
                <label for="subscription_length_12month">
                    <input type="checkbox" name="subscription_length_12month" id="subscription_length_12month" value="1"
                        <?php if (in_array(12, $defaults['subscription_lengths'])) echo 'checked="checked"'; ?> />
                    12 Month
                </label>
            </div>

            <div class="field">
                <label for="not_renewed">
                    <input type="checkbox" name="not_renewed" id="not_renewed" value="1" <?php if ($defaults['not_renewed']) echo 'checked="checked"'; ?> />
                    Not renewed or purchased since expiry (after subscription expired emails only)
                </label>
            </div>
        <?php }
    }

    public function interval_duration_min($min) {
        return $min = 0;
    }

}

$step2 = new Step2;