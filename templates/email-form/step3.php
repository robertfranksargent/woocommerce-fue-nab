<?php

class Step3 {

    public function __construct() {
        add_action('fue_email_form_before_message', array($this, 'email_form_before_message'), 10, 1 );
    }

    public function email_form_before_message($defaults) {
        ?>
        <div class="field">
            <label for="background">
                Email type:
                <select name="background" id="background">
                    <option value="C6D3D3" <?php selected( $defaults['background'], 'C6D3D3' ); ?>>Subscription</option>
                    <option value="E1DFFF" <?php selected( $defaults['background'], 'E1DFFF' ); ?>>Shop</option>
                    <option value="C8EAF5" <?php selected( $defaults['background'], 'C8EAF5' ); ?>>Dispatch</option>
                </select>
            </label>
        </div>
    <?php
    }

}

$step3 = new Step3;


