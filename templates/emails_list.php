<?php

class EmailsList {

    public function __construct() {
        add_action('fue_nab_emails_list_other_after_clone', array($this, 'emails_list_other_after_clone'), 10, 2 );

        add_action('email_types_script',  array($this, 'email_types_script'));
    }

    public function emails_list_other_after_clone($email, $key) {
        if ($key === 'subscription') { ?>
            <span class="edit"><a href="#" class="apply-to-existing-subscribers" data-id="<?php echo $email->id; ?>"><?php _e('Apply to existing', 'follow_up_emails_nab'); ?></a></span>
            |
        <?php }
    }

    public function email_types_script() {
        ?>
        // Apply to existing subscribers
        jQuery("a.apply-to-existing-subscribers").click(function(e) {
            e.preventDefault();

            var sure        = confirm("<?php _e('Are you sure you want to apply this email to existing subscriptions?', 'follow_up_emails_nab'); ?>");

            if (sure) {

                var email_id    = jQuery(this).data("id");
                var parent      = jQuery(this).parents("table");

                jQuery(parent).block({ message: null, overlayCSS: { background: '#fff url('+ FUE.ajax_loader +') no-repeat center', opacity: 0.6 } });

                var data = {
                    action: 'fue_nab_apply_to_existing_subscriptions',
                    id:     email_id,
                };

                jQuery.post(ajaxurl, data, function(resp) {
                    resp = JSON.parse(resp);

                    alert(resp.status + ': ' +resp.message);

                    jQuery(parent).unblock();

                });

            }

        });
        <?php
    }

}

$emailsList = new EmailsList;
