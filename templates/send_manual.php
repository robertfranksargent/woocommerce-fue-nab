<?php

class SendManual {

    public function __construct() {
        add_action('fue_manual_type_actions', array($this, 'manual_type_actions'), 20, 1 );

        add_action('fue_manual_js', array($this, 'manual_js'), 10, 1 );
    }

    public function manual_type_actions($email) {
        ?>

        <div id="subscription-options" style="display: none;">

            <div class="field">
                <label for="send_to"><?php _e('Who should the email be sent to?', 'follow_up_emails_nab'); ?></label>
                <select name="send_to" id="send_to">
                    <option value="buyer">Buyer</option>
                    <option value="recipient-or-buyer">Gift Recipient, Buyer if none</option>
                    <option value="recipient-only-or-buyer">Gift Recipient only</option>
                    <option value="webmaster@notanotherbill.com">Test: webmaster@notanotherbill.com</option>
                    <option value="nab@robertsandiford.co.uk">Test: nab@robertsandiford.co.uk</option>
                    <!--
                    <option value="recipient-or-buyer" <?php selected( $defaults['to'], 'recipient-or-buyer' ); ?>>Gift Recipient if gift, Buyer if empty. Buyer if for themself</option>
                    <option value="recipient-only-or-buyer" <?php selected( $defaults['to'], 'recipient-only-or-buyer' ); ?>>Gift Recipient if gift only. Buyer if for themself</option>
                    <option value="recipient" <?php selected( $defaults['to'], 'recipient' ); ?>>Gift Recipient if gift, Buyer if empty</option>
                    <option value="recipient-only" <?php selected( $defaults['to'], 'recipient-only' ); ?>>Gift Recipient if gift only</option>
                    -->
                </select>
            </div>

            <div class="field">
                <label for="subscription_recipient">
                    <?php _e('Subscription purchased for:', 'follow_up_emails_nab'); ?>
                </label>
                <select name="subscription_recipient" id="subscription_recipient">
                    <option value="">All</option>
                    <option value="themself">Themself</option>
                    <option value="gift">As a gift</option>
                </select>
            </div>

            <div class="field">
                <label>
                    <?php _e('Subscription length:', 'follow_up_emails_nab'); ?>
                </label>
                <label for="subscription_length_1month">
                    <input type="checkbox" name="subscription_length_1month" id="subscription_length_1month" value="1" checked="checked" />
                    1 Month Rolling
                </label>
                <label for="subscription_length_3month">
                    <input type="checkbox" name="subscription_length_3month" id="subscription_length_3month" value="1" checked="checked" />
                    3 Month
                </label>
                <label for="subscription_length_6month">
                    <input type="checkbox" name="subscription_length_6month" id="subscription_length_6month" value="1" checked="checked" />
                    6 Month
                </label>
                <label for="subscription_length_12month">
                    <input type="checkbox" name="subscription_length_12month" id="subscription_length_12month" value="1" checked="checked" />
                    12 Month
                </label>
            </div>

            <div class="field">
                <label for="not_renewed">
                    <input type="checkbox" name="not_renewed" id="not_renewed" value="1" />
                    Not renewed or purchased since expiry (after subscription expired emails only)
                </label>
            </div>

        </div>

        <?php
    }

    public function manual_js() {
        ?>
        var change = function() {
            if ( -1 != jQuery.inArray( jQuery(this).val() , [ "active_subscription", "expired_subscription", "expired_subscription_timeframe" ] ) ) {
                jQuery("#subscription-options").show();
            } else {
                jQuery("#subscription-options").hide();
            }
        }
        jQuery("#send_type").change(change);
        jQuery(change);

        jQuery("#expired_timeframe_from").datepicker({
            onClose: function( selectedDate ) {
                $( "#expired_timeframe_to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        jQuery("#expired_timeframe_to").datepicker();

        <?php
    }

}

$sendManual = new SendManual;